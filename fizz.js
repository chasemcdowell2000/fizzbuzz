function fizzBuzz(maxValue) {
    for (let x = 1; x <= maxValue; x++) {
        if (x % 2 == 0 & x % 3 == 0)
            console.log("FizzBuzz");
        else if (x % 2 == 0)
            console.log("Fizz");
        else if (x % 3 == 0)
            console.log("Buzz");
        else
            console.log(x);
    }
    return maxValue;
}
console.log(fizzBuzz(12))

function test_prime(n) {

    if (n === 1) {
        return false;
    }
    else if (n === 2) {
        return true;
    } else {
        for (var x = 2; x < n; x++) {
            if (n % x === 0) {
                return false;
            }
        }
        return " ";
    }
}

console.log(test_prime(37));

for (var counter = 0; counter <= 100; counter++) {

    var notPrime = false;
    for (var i = 2; i <= counter; i++) {
        if (counter % i === 0 && i !== counter) {
            notPrime = true;
        }
    }
    if (notPrime === false) { 
        console.log("Prime");
        
    }
}